﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addforceAtpos : MonoBehaviour
{
    public float force = 1f;
    // Start is called before the first frame update
    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.transform.tag =="Ball")
        collision.rigidbody.AddForceAtPosition(force*Vector3.forward, collision.gameObject.transform.position,ForceMode.Impulse);
    }
    }
