﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playsoundouttable : MonoBehaviour
{
    private AudioSource sound;
    private void Start()
    {
        sound = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            sound.Play();
        }

    }
}
