﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class warpBall : MonoBehaviour
{
    public GameObject ball;
    public Transform warpPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            ball.transform.position = new Vector3(99, 99, 99);
            yield return new WaitForSeconds(3);
            ball.transform.position = warpPos.transform.position;

        }
    }
    
       
}
