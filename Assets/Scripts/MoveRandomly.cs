﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveRandomly : MonoBehaviour
{
    private GameManager gmn;
    public GameObject blood;
    private AudioSource sound;
    public float time;
    public NavMeshAgent nav;
    bool inCorountine;
    Vector3 target;
    NavMeshPath path;
    bool validPath;
    // Start is called before the first frame update
    void Start()
    {
        sound = gameObject.GetComponent<AudioSource>();
        gmn = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        nav = gameObject.GetComponent<NavMeshAgent>();
        path = new NavMeshPath();
       
    }
    private void Update()
    {
        
        if (!inCorountine)
            StartCoroutine(Dosomthing());
    }
    Vector3 getnewRandomPosition()
    {
        float x = Random.Range(-20, 20);
        float z = Random.Range(-20, 20);
        Vector3 pos = new Vector3(x, 0, z);
        return pos;
    }

   IEnumerator Dosomthing()
    {
        inCorountine = true;
        yield return new WaitForSeconds(time);
        Getnewpath();
        validPath = nav.CalculatePath(target, path);

        while (!validPath)
        {
            yield return new WaitForSeconds(0.01f);
            Getnewpath();
            validPath = nav.CalculatePath(target, path);
        }
        inCorountine = false;
    }
    void Getnewpath()
    {
        target = getnewRandomPosition();
        nav.SetDestination(target);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            Instantiate(blood, transform.position, Quaternion.identity);
            Destroy(gameObject);
            if (collision.gameObject.CompareTag("Ball"))
                gmn.score += 500;
        }
    }
}
