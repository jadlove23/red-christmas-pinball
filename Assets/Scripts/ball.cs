﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour
{
    private GameManager gmn;
    private AudioSource sound;
    public GameObject ballprefebs;
    public GameObject spawnpoint;
    // Start is called before the first frame update
    void Start()
    {
        gmn = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        sound = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("outoftable"))
        {
            ResetPos();
            
        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bounce"))
        {
            sound.Play();
        }
        if (collision.gameObject.CompareTag("dirt"))
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.drag = 5;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("dirt"))
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.drag = 0;
        }
    }
    private void ResetPos()
    {
        ballprefebs.transform.position = spawnpoint.transform.position;
        gmn.life -= 1;
    }
}
