﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public  int life;
    public  int score;
    public Text scoreTxt;
    public Text scoreOverTxt;
    public Text lifeTxt;
    
    public GameObject gameOver;
    
    // Start is called before the first frame updat
    void Start()
    {
        Time.timeScale = 1f;
        life = 3;
        score = 0;
        scoreTxt.text = "Score: "+score.ToString();
        scoreOverTxt.text = "Score:  " + score.ToString();
        lifeTxt.text = "X  " + life.ToString();
       
    }

    // Update is called once per frame
    void Update()
    {
        scoreTxt.text = "Score: " + (score/10).ToString();
        lifeTxt.text = "X  " + life.ToString();
        scoreOverTxt.text = "Score:  " + (score / 10).ToString();
        if (life == 0)
        {
            GameOver();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Restart();
        }
    }
    public void Restart()
    {
        //Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void BacktoMEnu()
    {
        //Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }
    public void GameOver()
    {
        Time.timeScale = 0f;
        gameOver.SetActive(true);
    }
}
