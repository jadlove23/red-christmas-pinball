﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bounce : MonoBehaviour
{
    private GameManager gmn;
    public int PlusScore;
    public float explosionStrength = 100f;
    private void Start()
    {
        gmn = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }
    public void OnCollisionEnter(Collision collision)
    {
        collision.rigidbody.AddExplosionForce(explosionStrength, this.transform.position, 5);
        if(collision.gameObject.CompareTag("Ball"))
            gmn.score += PlusScore;
    }
}
