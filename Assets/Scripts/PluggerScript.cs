﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PluggerScript : MonoBehaviour
{
    float power;
    public float maxPower = 100f;
    public Slider powerSlider;
    List<Rigidbody> balllist;
    bool ballReady;
    // Start is called before the first frame update
    void Start()
    {
        powerSlider.maxValue = 0f;
        powerSlider.maxValue = maxPower;
        balllist = new List<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update()
    {
        powerSlider.value = power;
        if (balllist.Count > 0)
        {
            if (ballReady)
            {
                powerSlider.gameObject.SetActive(true);
            }
            else
            {
                powerSlider.gameObject.SetActive(false);
            }
            ballReady = true;
            if (Input.GetKey(KeyCode.Space))
            {
                if (power <= maxPower)
                {
                    power += 50 * Time.deltaTime;
                }
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
               foreach(Rigidbody r in balllist)
                {
                    r.AddForce(power * Vector3.forward);
                }
            }
        }
        else
        {
            ballReady = false;
            power = 0f;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            balllist.Add(other.gameObject.GetComponent<Rigidbody>());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            balllist.Remove(other.gameObject.GetComponent<Rigidbody>());
            power = 0f;
        }
    }
}
