﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    public GameObject Npc;
    public Vector3 center;
    public Vector3 size;
    public float timeCount;
    // Start is called before the first frame update
    void Start()
    {
        timeCount = Random.Range(5f, 8f);
    }

    // Update is called once per frame
    void Update()
    {
        timeCount -= Time.deltaTime;
        if (timeCount <= 0)
        {
            spawNPC();
            timeCount = Random.Range(5f, 8f);
        }

        
    }
    public void spawNPC()
    {
        Vector2 pos = center + new Vector3(Random.Range(-size.x/2 , size.x/2 ), Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z, size.z));
        Instantiate(Npc, pos, Quaternion.identity);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1,0,0,0.5f);
        Gizmos.DrawCube(center, size);
        
    }
}
