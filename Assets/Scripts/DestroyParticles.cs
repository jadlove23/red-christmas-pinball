﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticles : MonoBehaviour
{
    private AudioSource sound;
    public float destroyPar = 3f;
    // Start is called before the first frame update.
    private void Start()
    {
        sound = GetComponent<AudioSource>();
        sound.Play();
    }
    private void Update()
    {
        
        Destroy(gameObject, destroyPar);
    }
}
