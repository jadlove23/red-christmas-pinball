﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyTrapActor : MonoBehaviour
{
    FixedJoint _fixedJoint = null;
    public bool isJointing = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
            if (Input.GetKeyDown(KeyCode.Space)&& isJointing == true)
            {
                Debug.Log("relese");
                ReleaseFixedJoint();
            }


    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("glue"))
        {
            isJointing = true;
            GameObject go = other.gameObject;
            StickyTrap stickyTrap = go.GetComponent<StickyTrap>();
            Debug.Log("sticky");
            _fixedJoint = this.gameObject.AddComponent<FixedJoint>();
            _fixedJoint.connectedBody = other.gameObject.GetComponent<Rigidbody>();
        }


    }
    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("glue"))
        {
            isJointing = false;
        }
    }

    public void ReleaseFixedJoint()
    {
        _fixedJoint.breakForce = 1f;
        _fixedJoint.connectedBody=null;
        Destroy(this._fixedJoint);
    }
}
